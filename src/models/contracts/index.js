const bookshelf = require('./../../config/bookshelf');

const Contracts = bookshelf.Model.extend({
    tableName: 'contracts',
    idAttribute: 'address'
}, {

});

module.exports = Contracts;