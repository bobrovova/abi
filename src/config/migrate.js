var DBMigrate = require('db-migrate');
require('dotenv').config();

var dbmigrate = DBMigrate.getInstance();
dbmigrate.run();