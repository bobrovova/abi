const ContractsNamesParams = require('./../../models/contracts-namesparams');

module.exports = {
    createContractsNamesParams: async function(namesparamsId, addressContract){
        const contractsNamesParams = (new ContractsNamesParams({
            namesparams_id: namesparamsId,
            contract_id: addressContract
        })).save(null, {method: 'insert'}).catch(function(error){
            return error;
        });

        return contractsNamesParams;
    }
}