const bookshelf = require('./../../config/bookshelf');
const getHashBySignature = require('./../../utils/get-hash-by-signature').getHashBySignature;
const NamesParams = require('./../namesparams');
const Contracts = require('./../contracts');
const ContractsNamesParams = require('./../contracts-namesparams');

const Signature = bookshelf.Model.extend({
    tableName: 'signatures',
    idAttribute: 'hash',
    namesparams: function(){
        return this.hasMany(NamesParams, 'hash_signature', 'hash');
    }
}, {
    createByABI: (abi) => {
        let inputs = abi.inputs;
        let typeList = inputs.map((input, i, arr) => {
            return input.type;
        });
        let paramList = inputs.map((input, i, arr) => {
            return input.name;
        });

        let hash = getHashBySignature(abi.name, typeList);

        return new Signature({
            hash: hash,
            name: abi.name,
            typeList: JSON.stringify(typeList)
        })
            .save(null, {method: 'insert'})
            .then(async (model) => {
                let params = await new NamesParams({
                    hash_signature: hash,
                    paramList: JSON.stringify(paramList)
                }).save(null, {method: 'insert'});

                return model;
            });
    }
});

module.exports = Signature;