const web3 = require('./../config/web3');
const sgnToStr = require('./sgn-to-str');

function getHashBySignature(name, typeList){
    let hash = web3.sha3(sgnToStr(name, typeList)).substr(0, 10);
    return hash;
};

module.exports = { getHashBySignature };

