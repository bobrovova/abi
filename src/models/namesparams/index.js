const bookshelf = require('./../../config/bookshelf');
const Signature = require('./../signature');

const NamesParams = bookshelf.Model.extend({
    tableName: 'namesparams',
    signature: function() {
        return this.hasOne(Signature, 'hash_signature', 'hash');
    },
}, {

});

module.exports = NamesParams;