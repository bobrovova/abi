const Signature = require('./../../models/signature');
const _ = require('lodash');
const getHashBySignature = require('./../../utils/get-hash-by-signature').getHashBySignature;
const NamesParamsRepository = require('./../namesparams');
const ContractsNamesParamsRepository = require('./../contracts-namesparams');
const ContractsRepository = require('./../contracts');

module.exports = {
    findByHash: async function(hashSignature){
        let signature = await Signature.where('hash', hashSignature).fetch();
        return signature;
    },
    createSignature: async function(name, typeList){
        const hash = getHashBySignature(name, typeList);

        let signature = await (new Signature({
            hash: hash,
            name: name,
            typeList: JSON.stringify(typeList)
        })).save(null, {method: 'insert'}).catch(function(error){
            return error;
        });

        return signature;
    },
    createSignatureWithNamesparams: async function(name, paramList){
        const typeList = paramList.map((item, i, arr) => {
            return item.type;
        });

        const namesParamsList = paramList.map((item, i, arr) => {
            return item.name;
        });

        let signature = await this.createSignature(name, typeList);

        if(signature.name != undefined && signature.name == 'error'){
            return signature;
        } else {
            const namesParams = await NamesParamsRepository.createNamesParams(
                signature.hash,
                namesParamsList
            );

            if(namesParams.name != undefined && namesParams.name == 'error'){
                return namesParams;
            } else {
                signature = signature.toJSON();
                signature['namesparams'] = namesParams;

                return signature;
            }
        }
    },
    createBySource: async function(sourceCode, addressContract){
        const regexp = /function ([a-zA-Z]+)\(((?:(?:\n|\s|)+(?:[a-z0-9]+)\s(?:[_a-zA-Z0-9]+)(?:,|\s|))*)\)/g;
        let result;
        let functions = [];
        while (result = regexp.exec(sourceCode)){
            let signatureParams = result[2].split(',').map((item, i ,arr) => {
                return item.trim().split(' ');
            });

            let typeList = signatureParams.map((item, i, arr) => {
                return item[0];
            });

            let namesParamsList = signatureParams.map((item, i, arr) => {
                return item[1];
            });

            let signature = {
                name: result[1],
                typeList: typeList,
                namesParamsList: namesParamsList
            };

            functions.push(signature);
        }

        const contract = await ContractsRepository.findOrCreate(addressContract);

        if(contract.name != undefined && contract.name == 'error'){
            return contract;
        } else {
            return await Promise.all(functions.map(async(item, i, arr) => {
                let signature = await this.createSignature(item.name, item.typeList);

                if(signature.name != undefined && signature.name == 'error'){
                    return signature;
                } else {
                    const namesParams = await NamesParamsRepository.createNamesParams(
                        signature.hash,
                        item.namesParamsList
                    );
                    if(namesParams.name != undefined && namesParams.name == 'error'){
                        return namesParams;
                    } else {
                        const contractsNamesParams = await ContractsNamesParamsRepository.createContractsNamesParams(
                            namesParams.get('id'),
                            addressContract
                        );

                        if(contractsNamesParams.name != undefined && contractsNamesParams.name == 'error'){
                            return contractsNamesParams;
                        } else {
                            signature = signature.toJSON();
                            signature['namesparams'] = namesParams;

                            return signature;
                        }
                    }
                }
            }));
        }
    }
};