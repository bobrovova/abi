const Contracts = require('./../../models/contracts');

module.exports = {
    createContract: async function(address){
        const contract = await (new Contracts({
            address: address
        })).save(null, {method: 'insert'}).catch(function(error){
            return error;
        });

        return contract;
    },
    findOrCreate: async function(address){
        const contract = await Contracts.where('address', address).fetch();

        if(contract != null){
            return contract;
        } else {
            return await this.createContract(address);
        }
    }
}