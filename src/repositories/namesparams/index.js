const NamesParams = require('./../../models/namesparams');

module.exports = {
    createNamesParams: async function(hash, paramList){
        let namesParams = await (new NamesParams({
            hash_signature: hash,
            paramList: JSON.stringify(paramList)
        })).save(null, {method: 'insert'}).catch(function(error){
            return error;
        });

        return namesParams;
    }
}
