const bookshelf = require('./../../config/bookshelf');
const NamesParams = require('./../namesparams');
const Signature = require('./../signature');

const ContractsNamesParams = bookshelf.Model.extend({
    tableName: 'contracts-namesparams',
    namesparams: function(){
        return this.hasOne(NamesParams, 'namesparams_id', 'id');
    },
    signature: function(){

    }
}, {

});

module.exports = ContractsNamesParams;