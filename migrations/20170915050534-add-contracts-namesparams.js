'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createTable('contracts-namesparams', {
    columns: {
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      contract_id: {
        type: 'string'
      },
      namesparams_id: {
        type: 'int'
      }
    },
    ifNotExists: true
  });

  return null;
};

exports.down = function(db) {
  return db.dropTable('contracts-namesparams');
};

exports._meta = {
  "version": 1
};
