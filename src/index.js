const express       = require('express');
const app           = express();
const bodyParser    = require('body-parser');
const port = 8000;

require('dotenv').config();

app.set('view options', {
    layout: false
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.use(require('./controllers/api'));

app.listen(port, () => {
    console.log('Server has been started');
});

module.exports = app;