const app       = (require('express').Router)();
const Signature = require('./../../models/signature');
const Contracts = require('./../../models/contracts');
const ContractsNamesParams = require('./../../models/contracts-namesparams');
const SignatureRepository = require('./../../repositories/signature');

app.get('/:hash', async (req, res) => {
    const hash = req.params.hash;
    const result = await SignatureRepository.findByHash(hash);
    if(result != null){
        res.json(result);
    } else {
        res.statusCode = 404;
        res.json({
            'error': 'Not found'
        });
    }
});

app.get('/:hash/:addressContract', async (req, res) => {
    const hash = req.params.hash;
    const addressContract = req.params.addressContract;

    let result = await ContractsNamesParams.where(
        'contract_id', addressContract
    )   .with('namesparams')
        .whereHas( 'namesparams', (q) => {
            q.where('hash_signature', hash);
        })
        .first();

    result = result.toJSON();

    let signature = await Signature.where(
        'hash', result.namesparams.hash_signature
    ).fetch();

    res.json({
        contract: result,
        signature: signature
    });
});

app.post('/typeList', async (req, res) => {
    const params = req.body;

    const result = await SignatureRepository.createSignature(params.name, params.typeList);

    if(result.name != undefined && result.name == 'error'){
        res.statusCode = 500;
        res.send(result);
    } else {
        res.json(result);
    }
});

app.post('/source', async (req, res) => {
    const sourceCode = req.body.source;
    const addressContract = req.body.addressContract;
    let signatures = await SignatureRepository.createBySource(sourceCode, addressContract);

    res.send(signatures);
});

app.post('/abi', async (req, res) => {
    const abi = req.body.abi;
    const addressContract = req.body.addressContract;

    if(Array.isArray(abi)){
        const signatures = abi.map(async (item, i, arr) => {
            return (await Signature.createByABI(item)).toJSON();
        });
        res.json(signatures);
    } else {
        const signature = await Signature.createByABI(abi);
        res.json(signature);
    }

    if(addressContract != undefined){
        let contract = await (new Contracts({
            address: addressContract
        })).save();

        new ContractsNamesParams({
            contract_id: addressContract,
        })
    }
});

app.post('/paramList', async (req, res) => {
    const params = req.body;

    const result = await SignatureRepository.createSignatureWithNamesparams(
        params.name,
        params.paramsList
    );

    if(result.name != undefined && result.name == 'error'){
        res.statusCode = 500;
        res.send(result);
    } else {
        res.json(result);
    }
});

module.exports = app;